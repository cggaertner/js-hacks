if(!Function.prototype.bind) Function.prototype.bind = (function() {
	var	slice = Array.prototype.slice;

	return function(self /*, args... */) {
		var	fn = this,
			args = slice.call(arguments, 1);

		return function() {
			return fn.apply(self, args.concat(slice.call(arguments, 0)));
		};
	};
})();