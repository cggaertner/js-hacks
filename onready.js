function onready(/* listeners... */) {
	var	listeners = arguments,
		count = arguments.length,
		timeout = onready.timeout,
		lastChild = null;

	function poll() {
		if(document.body && document.body.lastChild &&
			document.body.lastChild !== lastChild) {
			for(var i = 0; i < count; ++i)
				listeners[i]();

			lastChild = document.body.lastChild;
		}

		if(!onready.loaded)
			setTimeout(poll, timeout);
	}

	setTimeout(poll, 0);
}

onready.timeout = 100;
onready.loaded = false;

try {
	document.addEventListener('DOMContentLoaded', function() {
		onready.loaded = true;
		document.removeEventListener('DOMContentLoaded',
			arguments.callee, false);
	}, false);
}
catch(e) {
	window.onload = function() {
		onready.loaded = true;
		window.onload = null;
	};
}