var mixin = (function() {
	function copy(dest, src) {
		for(var prop in src) {
			if(src.hasOwnProperty(prop))
				dest[prop] = src[prop];
		}
	}

	return function(/*mixins...*/) {
		var mixins = arguments;
		return function() {
			for(var i = 0; i < mixins.length; ++i)
				copy(this, mixins[i]);
		};
	};
})();