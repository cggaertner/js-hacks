(function() {
	var img = new Image;
	with(img.style) {
		position = 'absolute';
		cursor = 'pointer';
	}

	img.onload = show;
	img.onclick = hide;

	function load() {
		document.body.style.cursor = 'wait';
		img.style.display = 'none';
		img.src = this.href;
		return false;
	}

	function show() {
		this.style.border = minibox.borderSize + 'px ' + minibox.borderType;
		if(window.opera) this.style.display = '';
		this.style.top = document.body.scrollTop + Math.max(0,
			(document.body.clientHeight - this.height) / 2 -
			minibox.borderSize) + 'px';
		this.style.left = document.body.scrollLeft + Math.max(0,
			(document.body.clientWidth - this.width) / 2 -
			minibox.borderSize) + 'px';
		this.style.display = '';
		document.body.style.cursor = '';
		document.body.appendChild(this);
	}

	function hide() {
		this.parentNode.removeChild(this);
	}

	function init(regex) {
		if(!arguments.length) regex = /\.(jpg|png|gif)$/i;
		for(var i = document.links.length; i--;) {
			if(regex.test(document.links[i].href))
				document.links[i].onclick = load;
		}
	}

	minibox = init;
	minibox.load = load;
	minibox.img = img;
	minibox.borderSize = 0;
	minibox.borderType = 'none';
})();