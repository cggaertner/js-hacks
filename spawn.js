function spawn(base, fn) {
	fn.prototype = base.prototype;
	return new fn;
}
