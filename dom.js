function dom() {
	var frag = document.createDocumentFragment();
	frag.activeNode = frag;
	frag.open = dom._open;
	frag.close = dom._close;
	frag.text = dom._text;
	return frag;
}

dom._open = function(name) {
	var element = document.createElement(name);

	for(var i = 1; i < arguments.length; ++i) {
		var tokens = arguments[i].split('=', 2);
		element.setAttribute(tokens[0], tokens[1]);
	}

	this.activeNode = this.activeNode.appendChild(element);
	return this;
};

dom._close = function() {
	this.activeNode = this.activeNode.parentNode;
	return this;
};

dom._text = function(text) {
	this.activeNode.appendChild(document.createTextNode(text));
	return this;
};