if(typeof clone !== 'function')
	throw new Error('`clone-extensions` requires `clone`');

clone.withProto = function(obj) {
	var clonedObj = clone(obj);
	clonedObj.__proto__ = obj;
	return clonedObj;
};

clone.withProperties= function(obj, props, setProto) {
	var clonedObj = setProto ? clone.withProto(obj) : clone(obj);

	for(var name in props) {
		if(props.hasOwnProperty(name))
			clonedObj[name] = props[name];
	}

	return clonedObj;
};