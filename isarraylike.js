function isArrayLike(obj, checkFirstEntry) {
	return typeof obj !== 'undefined' && obj !== null &&
		typeof obj.length === 'number' &&
		(!checkFirstEntry || typeof obj[0] !== 'undefined');
}