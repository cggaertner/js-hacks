// Copyright 2011 Christoph Gärtner
// Distributed under the Boost Software License, Version 1.0
// See <http://www.boost.org/LICENSE_1_0.txt> for details

(function() {

	this.MONKEY = wrap;
	this.MONKEY.unwrap = unwrap;
	this.MONKEY.patch = patch;

	function wrap(obj) {
		return new Wrapper(Object(obj));
	}

	function unwrap(obj) {
		return obj.MONKEY__self__;
	}

	function patch(func) {
		if(!func.hasOwnProperty('MONKEY__patch__'))
			func.MONKEY__patch__ = {};

		return func.MONKEY__patch__;
	}

	function Wrapper(self) {
		this.MONKEY__self__ = self;
		this.MONKEY__patch__ = patch(self.constructor);
	}

	function delegate(id) {
		return function() {
			var self = this.MONKEY__self__;
			return self[id].apply(self, arguments);
		};
	}

	Wrapper.prototype.__noSuchMethod__ = function(id, args) {
		return this.MONKEY__patch__.hasOwnProperty(id) ?
			this.MONKEY__patch__[id].apply(this, args) :
			this.MONKEY__self__[id].apply(this.MONKEY__self__, args);
	};

	Wrapper.prototype.hasOwnProperty = delegate('hasOwnProperty');
	Wrapper.prototype.isPrototypeOf = delegate('isPrototypeOf');
	Wrapper.prototype.propertyIsEnumerable = delegate('propertyIsEnumerable');
	Wrapper.prototype.toLocaleString = delegate('toLocaleString');
	Wrapper.prototype.toString = delegate('toString');
	Wrapper.prototype.valueOf = delegate('valueOf');

})();
