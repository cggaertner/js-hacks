function hash(value) {
	return value instanceof Object ? (value.__hash ||
		(value.__hash = 'object ' + ++arguments.callee.current)) :
		(typeof value) + ' ' + String(value);
}

hash.current = 0;