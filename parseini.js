function parseINI(data) {
	var store = {}, section = store;
	data = data.split('\n');

	for(var i = 0, len = data.length; i < len; ++i) {
		var current = data[i].split(';', 2)[0].replace(/^\s+|\s+$/g, '');

		if(!current.length)
			continue;

		if(current.charAt(0) === '[') {
			var name = current.substring(1, current.indexOf(']'));
			if(!store.hasOwnProperty(name))
				store[name] = {};
			section = store[name];
		}
		else {
			var tokens = current.split(/\s*=\s*/, 2);
			section[tokens[0]] = tokens[1];
		}
	}

	return store;
}