function typeOf(value) {
	var type = typeof value;

	switch(type) {
		case 'object':
		return value === null ? 'null' : Object.prototype.toString.call(value).
			match(/^\[object (.*)\]$/)[1]

		case 'function':
		return 'Function';

		default:
		return type;
	}
}