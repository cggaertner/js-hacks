function each(obj, func, thisArg) {
	if(typeof thisArg === 'undefined')
		thisArg = obj;

	for(var prop in obj) {
		if(obj.hasOwnProperty(prop))
			func.call(thisArg, prop, obj[prop]);
	}

	return obj;
}