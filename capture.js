// Copyright 2011 Christoph Gärtner
// Distributed under the Boost Software License, Version 1.0
// See <http://www.boost.org/LICENSE_1_0.txt> for details

var capture = (function() {

	function throwError() {
		throw new Error('event capture unsupported');
	}

	function checkTrue() {
		return true;
	}

	function checkRegExp(element) {
		return typeof element.className !== 'undefined' &&
			this.regex.test(element.className);
	}

	function checkFunc(element) {
		return this.func.apply(element);
	}

	function checkSelector(element) {
		if(element.nodeType !== 1)
			return false;

		if(this.tagName !== '*' &&
			element.tagName.toLowerCase() !== this.tagName.toLowerCase())
			return false;

		if(this.id && element.id !== this.id)
			return false;

		for(var i = this.classes.length; i--; ) {
			if(!this.classes[i].test(element.className))
				return false;
		}

		return true;
	}

	function checkList(element) {
		for(var i = 0, len = this.list.length; i < len; ++i) {
			if(this.list[i].check(element))
				return true;
		}

		return false;
	}

	function Checker(condition) {
		if(condition === true)
			this.check = checkTrue;

		else if(typeof condition === 'string') {
			if(condition.indexOf(',') >= 0)
				return new Checker(condition.split(/,\s*/));

			var	tokens = condition.split('.'),
				tmp = tokens[0].split('#', 2);

			this.tagName = tmp[0] || '*';
			this.id = tmp[1] || false;
			this.classes = [];

			for(var i = 1; i < tokens.length; ++i) {
				this.classes.push(
					new RegExp('(^|\\s)' + tokens[i] + '(\\s|$)'));
			}

			this.check = checkSelector;
		}

		else if(condition instanceof Array) {
			this.list = [];

			for(var i = condition.length; i--; )
				this.list[i] = new Checker(condition[i]);

			this.check = checkList;
		}

		else if(condition instanceof RegExp) {
			this.regex = condition;
			this.check = checkRegExp;
		}

		else if(typeof condition === 'function') {
			this.func = condition;
			this.check = checkFunc;
		}

		else throw new Error('unsupported capture condition: ' + condition);
	}

	var add =
		document.addEventListener ? function(type, listener) {
			document.addEventListener(type, listener, false);
		} :
		document.attachEvent ? function(type, listener) {
			document.attachEvent('on' + type, listener);
		} :
		throwError;

	var remove =
		document.removeEventListener ? function(type, listener) {
			document.removeEventListener(type, listener, false);
		} :
		document.detachEvent ? function(type, listener) {
			document.detachEvent('on' + type, listener);
		} :
		throwError;

	function createListener(checker, handler) {
		return function(event) {
			var	event = event || window.event,
				target = event.target || event.srcElement;

			for(; target; target = target.parentNode) {
				if(checker.check(target)) {
					if(handler.call(target, event) === false) {
						if(event.preventDefault) event.preventDefault();
						else event.returnValue = false;
					}

					break;
				}
			}
		};
	}

	function capture(types, condition, handler) {
		var	types = types.split(/,\s*/),
			checker = new Checker(condition),
			listener = createListener(checker, handler);

		listener.types = types;

		for(var i = types.length; i--; )
			add(types[i], listener);

		return listener;
	}

	capture.remove = function(listener) {
		for(var i = listener.types.length; i--; )
			remove(listener.types[i], listener);
	}

	return capture;

})();
