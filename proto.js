var Proto = new (function() {
	function Dummy() {}

	this.clone = function() {
		Dummy.prototype = this;
		return new Dummy;
	};

	this.init = function() {};

	this.create = function() {
		var obj = this.clone();
		this.init.apply(obj, arguments);
		return obj;
	};

	this.from = function(props) {
		var obj = this.clone();
		for(var name in props) {
			if(props.hasOwnProperty(name))
				obj[name] = props[name];
		}
		return obj;
	};
});