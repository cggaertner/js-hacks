(function() {
	function diffStrings(s1, s2, regex, glue) {
		var	list = this(s1.split(regex || /\b/), s2.split(regex || /\b/)),
			stringList = [];

		if(typeof glue === 'undefined')
			glue = '';

		for(var i = 0; i < list.length; ++i) {
			var string = new String(list[i].join(glue));
			string.type = list[i].type;
			stringList.push(string);
		}

		stringList.toHtml = toHtml;
		stringList.glue = glue;
		return stringList;
	}

	function toHtml() {
		var html = '';

		for(var i = 0; i < this.length; ) {
			html += toHtml[this[i].type](this[i]) +
				(++i === this.length ? '' : this.glue);
		}

		return html;
	}

	toHtml['='] = escape;

	toHtml['+'] = function(string) {
		return '<ins>' + escape(string) + '</ins>';
	};

	toHtml['-'] = function(string) {
		return '<del>' + escape(string) + '</del>';
	};

	function escape(string) {
		return string.replace(/&/g, '&amp;').replace(/</g, '&lt;').
			replace(/>/g, '&gt;');
	}

	if(typeof diff === 'function')
		diff.strings = diffStrings;

	if(typeof fastDiff === 'function')
		fastDiff.strings = diffStrings;
})();