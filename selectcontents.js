var selectContents = (function() {
	function select_get(element) {
		// would be shorter, but doesn't work in Opera:
		/*	var selection = window.getSelection();
			selection.selectAllChildren(elem);*/

		var range = document.createRange();
		range.selectNode(element);
		var selection = window.getSelection();
		selection.removeAllRanges();
		selection.addRange(range);
	}

	function select_move(element) {
		var range = document.selection.createRange();
		range.moveToElementText(element);
		range.select();
	}

	return (document.createRange && window.getSelection) ? select_get :
		(document.selection && document.selection.createRange) ? select_move :
		false;
})();