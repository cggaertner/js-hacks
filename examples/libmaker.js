// Copyright 2011 Christoph Gärtner
// Distributed under the Boost Software License, Version 1.0
// See <http://www.boost.org/LICENSE_1_0.txt> for details

var capture = (function() {

	function throwError() {
		throw new Error('event capture unsupported');
	}

	function checkTrue() {
		return true;
	}

	function checkRegExp(element) {
		return typeof element.className !== 'undefined' &&
			this.regex.test(element.className);
	}

	function checkFunc(element) {
		return this.func.apply(element);
	}

	function checkSelector(element) {
		if(element.nodeType !== 1)
			return false;

		if(this.tagName !== '*' &&
			element.tagName.toLowerCase() !== this.tagName.toLowerCase())
			return false;

		if(this.id && element.id !== this.id)
			return false;

		for(var i = this.classes.length; i--; ) {
			if(!this.classes[i].test(element.className))
				return false;
		}

		return true;
	}

	function checkList(element) {
		for(var i = 0, len = this.list.length; i < len; ++i) {
			if(this.list[i].check(element))
				return true;
		}

		return false;
	}

	function Checker(condition) {
		if(condition === true)
			this.check = checkTrue;

		else if(typeof condition === 'string') {
			if(condition.indexOf(',') >= 0)
				return new Checker(condition.split(/,\s*/));

			var	tokens = condition.split('.'),
				tmp = tokens[0].split('#', 2);

			this.tagName = tmp[0] || '*';
			this.id = tmp[1] || false;
			this.classes = [];

			for(var i = 1; i < tokens.length; ++i) {
				this.classes.push(
					new RegExp('(^|\\s)' + tokens[i] + '(\\s|$)'));
			}

			this.check = checkSelector;
		}

		else if(condition instanceof Array) {
			this.list = [];

			for(var i = condition.length; i--; )
				this.list[i] = new Checker(condition[i]);

			this.check = checkList;
		}

		else if(condition instanceof RegExp) {
			this.regex = condition;
			this.check = checkRegExp;
		}

		else if(typeof condition === 'function') {
			this.func = condition;
			this.check = checkFunc;
		}

		else throw new Error('unsupported capture condition: ' + condition);
	}

	var add =
		document.addEventListener ? function(type, listener) {
			document.addEventListener(type, listener, false);
		} :
		document.attachEvent ? function(type, listener) {
			document.attachEvent('on' + type, listener);
		} :
		throwError;

	var remove =
		document.removeEventListener ? function(type, listener) {
			document.removeEventListener(type, listener, false);
		} :
		document.detachEvent ? function(type, listener) {
			document.detachEvent('on' + type, listener);
		} :
		throwError;

	function createListener(checker, handler) {
		return function(event) {
			var	event = event || window.event,
				target = event.target || event.srcElement;

			for(; target; target = target.parentNode) {
				if(checker.check(target)) {
					if(handler.call(target, event) === false) {
						if(event.preventDefault) event.preventDefault();
						else event.returnValue = false;
					}

					break;
				}
			}
		};
	}

	function capture(types, condition, handler) {
		var	types = types.split(/,\s*/),
			checker = new Checker(condition),
			listener = createListener(checker, handler);

		listener.types = types;

		for(var i = types.length; i--; )
			add(types[i], listener);

		return listener;
	}

	capture.remove = function(listener) {
		for(var i = listener.types.length; i--; )
			remove(listener.types[i], listener);
	}

	return capture;

})();
 
 
var create = (function() {

	var fallback = {
		'class' : 'className',
		'for' : 'htmlFor',
		'name' : 'name'
	};

	function create(name, attributes) {
		return set(document.createElement(name), attributes, arguments);
	}

	function set(element, attributes, args) {
		var fallback = create.fallback;

		for(var aName in attributes) {
			if(attributes.hasOwnProperty(aName)) {
				var value = attributes[aName];

				if(typeof value === 'string') {
					if(fallback.hasOwnProperty(aName))
						element[fallback[aName]] = value;
					else element.setAttribute(aName, value);
				}
				else if(value instanceof Object) {
					if(fallback.hasOwnProperty(aName))
						aName = fallback[aName];

					for(var prop in value) {
						if(value.hasOwnProperty(prop))
							element[aName][prop] = value[prop];
					}
				}
				else throw new Error('illegal attribute: ' + value);
			}
		}

		return append(element, args, 2);
	};

	function createFrom(html, attributes) {
		if(attributes === false)
			return document.createTextNode(html);

		var element = document.createElement('div');
		element.innerHTML = html;

		var	nodes = element.childNodes;
		for(var i = nodes.length; i--; )
			set(nodes[i], attributes, []);

		var fragment = document.createDocumentFragment();
		while(nodes.length)
			fragment.appendChild(element.removeChild(element.lastChild));

		return fragment;
	}

	function append(element, nodes, offset) {
		for(var i = offset || 0, len = nodes.length; i < len; ++i)
			element.appendChild(nodes[i]);

		return element;
	}

	create.fallback = fallback;
	create.from = createFrom;
	create.appendix = append;

	return create;
})(); 
 
if(typeof create !== 'function')
	throw new Error('`create-elements` requires `create`');

create.br = function() {
	return create('br');
};

create.div = function() {
	return create.appendix(create('div'), arguments)
};

create.fieldset = function(name) {
	var set = create('fieldset', null, create.legend(name));
	create.appendix(set, arguments, 1);
	return set;
};

create.h1 = function() {
	return create.appendix(create('h1'), arguments)
};

create.input = function(type, name, value) {
	var input = create('input', { 'type' : type });
	if(name) input.name = name;
	if(value) input.value = value;
	return input;
};

create.label = function(forId) {
	return create.appendix(create('label', { 'for' : forId }), arguments, 1);
};

create.legend = function(text) {
	return create('legend', null, create.from(text, false));
};

create.span = function() {
	return create.appendix(create('span'), arguments);
};

create.text = function(text) {
	return create.from(text, false);
};

create.fragment = function() {
	return create.appendix(document.createDocumentFragment(), arguments);
}; 
 
function each(obj, func, thisArg) {
	if(typeof thisArg === 'undefined')
		thisArg = obj;

	for(var prop in obj) {
		if(obj.hasOwnProperty(prop))
			func.call(thisArg, prop, obj[prop]);
	}

	return obj;
} 
 
function escape(string) {
	return string.replace(/&/g, '&amp;').replace(/</g, '&lt;').
		replace(/>/g, '&gt;');
} 
 
function getHttpResource(url)  {
	var req = new XMLHttpRequest;
	if(!req) return null;

	try {
		req.open('GET', url, false);
		req.send(null);

		return req.responseText;
	}
	catch(e) {
		return null;
	}
} 
 
var isIE = /*@cc_on @if(@_jscript) !@end @*/false; 
 
function onready(/* listeners... */) {
	var	listeners = arguments,
		count = arguments.length,
		timeout = onready.timeout,
		lastChild = null;

	function poll() {
		if(document.body && document.body.lastChild &&
			document.body.lastChild !== lastChild) {
			for(var i = 0; i < count; ++i)
				listeners[i]();

			lastChild = document.body.lastChild;
		}

		if(!onready.loaded)
			setTimeout(poll, timeout);
	}

	setTimeout(poll, 0);
}

onready.timeout = 100;
onready.loaded = false;

try {
	document.addEventListener('DOMContentLoaded', function() {
		onready.loaded = true;
		document.removeEventListener('DOMContentLoaded',
			arguments.callee, false);
	}, false);
}
catch(e) {
	window.onload = function() {
		onready.loaded = true;
		window.onload = null;
	};
} 
 
function query(selector, parent) {
	if(selector.charAt(0) === '#')
		return document.getElementById(selector.substring(1));

	if(!parent)
		parent = document;

	if(selector.indexOf('.') < 0)
		return parent.getElementsByTagName(selector);

	var	tokens = selector.split('.'),
		tagName = tokens.unshift() || '*',
		elements = parent.getElementsByTagName(tagName),
		regs = [],
		matchingElements = [];

	for(var i = 0; i < tokens.length; ++i)
		regs.push(eval('/(^|\s)' + tokens[i] + '(\s|$)/'));

	loop:
	for(var i = 0; i < elements.length; ++i) {
		var element = elements[i];

		if(element.nodeType != 1)
			continue;

		for(var j = 0; j < regs.length; ++j) {
			if(!regs[j].test(element.className))
				continue loop;
		}

		matchingElements.push(element);
	}

	return matchingElements;
} 
 
var selectContents = (function() {
	function select_get(element) {
		// would be shorter, but doesn't work in Opera:
		/*	var selection = window.getSelection();
			selection.selectAllChildren(elem);*/

		var range = document.createRange();
		range.selectNode(element);
		var selection = window.getSelection();
		selection.removeAllRanges();
		selection.addRange(range);
	}

	function select_move(element) {
		var range = document.selection.createRange();
		range.moveToElementText(element);
		range.select();
	}

	return (document.createRange && window.getSelection) ? select_get :
		(document.selection && document.selection.createRange) ? select_move :
		false;
})(); 
 
if(	typeof window.XMLHttpRequest === 'undefined' &&
	typeof window.ActiveXObject === 'function') {
	window.XMLHttpRequest = function() {
		try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e) {}
		try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e) {}
		return new ActiveXObject('Microsoft.XMLHTTP');
	};
}