var IPv4 = (new function() {
	var regex = /^(25[0-5]|(2[0-4]|1[0-9])[0-9]|[0-9]{1,2})$/;

	function isValid(string) {
		var tokens = string.split('.');
		return tokens.length === 4 &&
			regex.test(tokens[0]) && regex.test(tokens[1]) &&
			regex.test(tokens[2]) && regex.test(tokens[3]);
	}

	function valueOf(string) {
		var bytes = string.split('.');
		return (((bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) |
			bytes[3]) + 0x100000000) % 0x100000000;
	}

	function isCurrent(ip) {
		return
			(0x00000000 <= ip && ip <= 0x00FFFFFF);
	}

	function isPrivate(ip) {
		return
			(0x0A000000 <= ip && ip <= 0x0AFFFFFF) ||
			(0xAC100000 <= ip && ip <= 0xAC1FFFFF) ||
			(0xC0A80000 <= ip && ip <= 0xC0A8FFFF);
	}

	function isPublic(ip) {
		return
			(0x0E000000 <= ip && ip <= 0x0EFFFFFF);
	}

	function isLoopback(ip) {
		return
			(0x7F000000 <= ip && ip <= 0x7FFFFFFF);
	}

	function isReserved(ip) {
		return
			(0x80000000 <= ip && ip <= 0x8000FFFF) ||
			(0xBFFF0000 <= ip && ip <= 0xBFFFFFFF) ||
			(0xC0000000 <= ip && ip <= 0xC00000FF) ||
			(0xDFFFFF00 <= ip && ip <= 0xDFFFFFFF) ||
			(0xF0000000 <= ip && ip <= 0xFFFFFFFF);
	}

	function isLinkLocal(ip) {
		return
			(0xA9FE0000 <= ip && ip <= 0xA9FEFFFF);
	}

	function isExample(ip) {
		return
			(0xC0000200 <= ip && ip <= 0xC00002FF);
	}

	function isIPv6Relay(ip) {
		return
			(0xC0586300 <= ip && ip <= 0xC05863FF);
	}

	function isBenchmark(ip) {
		return
			(0xC6120000 <= ip && ip <= 0xC613FFFF);
	}

	function isMulticast(ip) {
		return
			(0xE0000000 <= ip && ip <= 0xEFFFFFFF);
	}

	function wrap(fn) {
		return function(string) {
			return fn(valueOf(string));
		};
	}

	function isBroadcast(ip, routing_bits) {
		var mask = 0xFFFFFFFF >> routing_bits;
		return (ip & mask) === mask;
	}

	this.isValid = isValid;
	this.valueOf = valueOf;
	this.isCurrent = wrap(isCurrent);
	this.isPrivate = wrap(isPrivate);
	this.isPublic = wrap(isPublic);
	this.isLoopback = wrap(isLoopback);
	this.isReserved = wrap(isReserved);
	this.isLinkLocal = wrap(isLinkLocal);
	this.isExample = wrap(isExample);
	this.isIPv6Relay = wrap(isIPv6Relay);
	this.isBenchmark = wrap(isBenchmark);
	this.isMulticast = wrap(isMulticast);
	this.isBroadcast = function(string, routing_bits) {
		return isBroadcast(valueOf(string), routing_bits || 0);
	};
});