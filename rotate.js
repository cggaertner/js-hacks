if(!Array.prototype.rotate) (function() {
	var	unshift = Array.prototype.unshift,
		splice = Array.prototype.splice;

	Array.prototype.rotate = function(count) {
		var	len = this.length >>> 0,
			count = count >> 0;

		unshift.apply(this, splice.call(this, count % len, len));
		return this;
	};
})();
