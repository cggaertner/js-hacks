var minify = (function() {
	function trim(string) {
		return string.replace(/(^\s+)|(\s+$)/g, '');
	}

	function stripNnl(string) {
		return string === '\n' ? '\n' : '';
	}

	function mapRange(source, start, end, func) {
		var dest = [];

		for(var i = start; i <= end; ++i)
			dest.push(func(source[i]));

		return dest;
	}

	function replaceEach(array, func) {
		for(var i = array.length; i--; )
			array[i] = func.call(this, array[i]);

		return array;
	}

	function skip(tokens, idx, limiter, escapes) {
		while(++idx < tokens.length && tokens[idx] !== limiter)
			if(escapes && tokens[idx] === '\\') ++idx;

		return idx;
	}

	return function(code) {
		var	regex = /\/\*|\*\/|\/\/|\\|["']|\+\+|--|\+=|-=|\*=|\/=|>>>=|>>=|<<=|&=|\|=|\^=|!==|!=|===|==|<=|>=|&&|\|\||[\+\-\*\/%&\|\^~<>=!\.\?:,;\(\)\{\}\[\]]|\n|\s+/mg,
			separators = /^[\+\-\*\/%&\|\^~<>=!\.\?:,;\(\)\{\}\[\]]+$/,
			tokens = [],
			pos = 0;

		code = replaceEach(code.split(/\n/g), trim).join('\n');

		for(var matches; matches = regex.exec(code); pos = regex.lastIndex) {
			var	match = matches[0],
				matchStart = regex.lastIndex - match.length;

			if(pos < matchStart)
				tokens.push(code.substring(pos, matchStart));

			tokens.push(match);
		}

		if(pos < code.length)
			tokens.push(code.substring(pos));

		for(var i = 0, j; i < tokens.length; ++i) {
			switch(tokens[i]) {
				case '/*':
				j = skip(tokens, i, '*/');
				tokens.splice(i, j - i + 1,
					mapRange(tokens, i, j, stripNnl).join(''));
				break;

				case '//':
				j = skip(tokens, i, '\n');
				tokens.splice(i, j - i);
				break;

				case '"':
				i = skip(tokens, i, '"', true);
				break;

				case '\'':
				i = skip(tokens, i, '\'', true);
				break;

				case '/':
				i = skip(tokens, i, '/', true);
				break;

				case '\n':
				break;

				default:
				if(/^\s+$/.test(tokens[i])) {
					if(!i || separators.test(tokens[i - 1]))
						tokens.splice(i--, 1);
					else tokens[i] = ' ';
				}
				else if(separators.test(tokens[i])) {
					if(i && /^\s+$/.test(tokens[i - 1]) &&
						tokens[i - 1] !== '\n') tokens.splice(--i, 1);
					else if(i && (
						(tokens[i - 1][tokens[i - 1].length - 1] === '+' &&
							tokens[i][0] === '+') ||
						(tokens[i - 1][tokens[i - 1].length - 1] === '-' &&
							tokens[i][0] === '-'))) tokens.splice(i++, 0, ' ');
				}
			}
		}

		return tokens.join('');
	};
})();