WARNING: this file is seriously out of date!

js-hacks build 2009.01-a1
    unobstrusive javascript scriptlets
    check <license.txt> for usage and distribution rights

    the current version can be obtained from
        <http://mercurial.intuxication.org/hg/js-hacks>


--- <capture.js> ---

capture(<type>, <condition>, <handler>) --> <listener>
    type        - event type string, eg. `'click'`
    condition   - condition argument (see below)
    handler     - handler function
    listener    - generated listener function; needed for listener removal

    supports a centralised, passive form of event handling: instead of adding
    listeners to each element to be listened to, a listener is once added to
    `document`

    this listener will catch any event of appropriate <type> which has not
    been prevented to reach `document` during the bubbling phase via
    `event.stopPropagation()` (read `event.returnValue = false` for MS IE)

    check <http://www.w3.org/TR/DOM-Level-3-Events/events.html#Events-flow>
    for details on the event flow; the name 'capture' for this function is
    actually  misleasing: it catches events at the end of the bubbling phase,
    not during the capture phase

    if the <condition> matches the target of an event which has been caught,
    the <handler> function will be executed with the target element as `this`
    and the event object as first argument (even in MS IE)

    if the <handler> function returns `false`, the default action of the event
    on its target (eg. following a link, submitting a form, ...) will be
    prevented

    the values of the following types are supported as <condition>-argument:
        - `true`, which means that every event - irrespective of its target -
            will trigger the <handler>

        - a string, which will trigger <handler> execution for targets with
            appropriate id if the string starts with '#', targets with given
            class if the string starts with '.' and targets with a matching
            tag name otherwise

        - a regular expression which will be matched against the target's
            `className`-attribute to determine if the <handler> ought to be
            called

        - a function to which the target element will be supplied as `this`;
            the <handler> will be called if the function's return value
            evaluates to `true`

    if <condition> is none of the above, an error will be thrown


capture.remove(<listener>)
    listener    - the listener to be removed

    removes a listener and stops capturing associated events


--- <clone.js> ---

clone(<obj>) --> <clonedObj>
    obj         - the object to be cloned
    clonedObj   - the object's clone

    creates a new object <clonedObj> whose internal [[Prototype]]-property
    will be set to <obj>

    additionally, its `__proto__`-property will be set to <obj> as well


--- <create.js> ---

TODO


--- <each.js> ---

each(<obj>, <func>) --> <obj>
    obj     - the object whose' properties will be iterated over
    func    - the function which will be called for each of <obj>'s
                properties

    iterates over the enumerable properties <prop> of <obj> for which
    `<obj>.hasOwnProperty(<prop>)` returns `true`

    <func> will be called with `this` set to <obj> and <prop> and
    `<obj>[<prop>]` as arguments


--- <hash.js> ---

hash(<value>) --> <hashStr>
    value   - a primitive or object to be hashed
    hashStr - a unique hash string

    computes a hash string for <value> which can be used as lookup-key when
    using objects as maps for arbitrary values

    discerns between primitives of different type, ie. `hash(5) !== hash('5')`

    adds a unique global id to objects in the `__hash`-property


--- <map.js> ---

new Map(<linkItems?>) --> <map>
    linkItems?  - optional argument; if `false`, the map won't be linked;
                    omitting or setting it to any other value enables linking
    <map>       - TODO

    creates a map object accepting arbitrary keys by using the hash function
    from <hash.js>

    if instructed to enable linking, the keys and values will be stored in a
    circular, doubly-linked list to allow fast iteration over its entries


Map.from(<obj>, <foreignKeys?>, <linkItems?>) --> <map>
    obj             - the object whose' properties will be added to <map>
    foreignKeys?    - optional argument;
    linkItems?      - optional argument;
    map             -

    TODO


<map>.disableLinking() --> <map>
    map - the `Map`-instance

    if called, the receiving map will stop adding entries to the linked list

    once disbaled, linking can't be enabled again; also, the following methods
    won't work anymore and will produce an error instead:
        `removeAll()`, `next()`, `key()`, `value()`, `each()`


<map>.isLinked --> <isLinked>
    map         - ...
    isLinked    - ...


<map>.size --> <size>
    map     -
    size    -

<map>.hash(<value>) --> <hashStr>
<map>.get(<key>) --> <value>
<map>.put(<key>, <value?>) --> <map>
<map>.remove(<key>) --> <map>
<map>.removeAll() --> <map>
<map>.contains(<key>) --> <bool>
<map>.next()
<map>.key() --> <currentKey>
<map>.value() --> <currentValue>
<map>.each(<func>) --> <map>
<map>.toString() --> <dbgStr>


--- <onready.js> ---

onready(<funcs...>)
    funcs...    - list of functions to be executed

    cross-browser alternative to the `DOMContentLoaded` event

    checks repeatedly for `document.body && document.body.lastChild` to
    determine whether the DOM has been built

    the timeout between consecutive checks can be set via `onready.timeout`
    before calling `onready()`

    the default timeout of `100` might lead to a small delay between page
    rendering and the functions' execution


--- <quote.js> ---

quote(<string>, <doubles?>, <nonAscii?>) --> <qString>
    string      - the string to be quoted
    doubles?    - optional argument; uses double quotes if <doubles?> evaluates
                    to `true`; single quotes are used by default
    nonAscii?   - optional argument; <qString> may contain non-ASCII characters
                    if <nonAscii?> evaluates to `true`; <qString> will be
                    ASCII-safe by default
    qString     - the <string> surrounded by quotes and with properly escaped
                    characters

    surrounds <string> with (single or double) quotes and adds escape sequences
    so that <qString> will be safe to use as a javascript string literal

    by default, <qString> will only contain displayable ASCII characters


--- <selectcontents.js> ---

TODO


--- <serialize.js> ---

serialize(<value>, <foreignProps?>, <noErrors?>) --> <string>
    value           -
    foreignProps?   -
    noErrors?       -
    string          -

    TODO; depends on quote.js


--- <shuffle.js> ---

shuffle(<array>) --> <array>
    array   - the array to be shuffled

    randomizes the order of an array's elements in place

    also works for array-like objects as long as they provide a
    `length`-property


--- <xmlhttprequest.js> ---

new XMLHttpRequest() --> <reqObj>
    reqObj  - the request object

    fixes a minor inconvenience with Microsoft Internet Explorer by adding a
    constructor function wrapping the ActiveX-calls for request objects

http://blogs.msdn.com/xmlteam/archive/2006/10/23/using-the-right-version-of-msxml-in-internet-explorer.aspx

http://blogs.msdn.com/ie/archive/2006/01/23/516393.aspx

http://msdn.microsoft.com/en-us/library/ms757837(VS.85).aspx

    for a far more sophisticated patch for the quirks of various browsers,
    check <http://code.google.com/p/xmlhttprequest/>


[EOF]