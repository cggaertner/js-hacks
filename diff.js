var diff = (function() {
	if(typeof Map !== 'function')
		throw new Error('`diff` requires `map`');

	function Sequence(type, oIndex, nIndex, length) {
		this.type = type;
		this.length = length;

		if(oIndex >= 0) {
			this.oStart = oIndex;
			this.oEnd = oIndex + length - 1;
			this.oNext = oIndex + length;
		}

		if(nIndex >= 0) {
			this.nStart = nIndex;
			this.nEnd = nIndex + length - 1;
			this.nNext = nIndex + length;
		}
	}

	Sequence.prototype.shift = function() {
		++this.oStart;
		++this.nStart;
		--this.length;

		return this.length > 0;
	}

	Sequence.prototype.pop = function() {
		--this.oEnd;
		--this.oNext;
		--this.nEnd;
		--this.nNext;
		--this.length;

		return this.length > 0;
	};

	Sequence.prototype.isLeftOf = function(seq) {
		return this.oEnd < seq.oStart && this.nEnd < seq.nStart;
	}

	Sequence.prototype.isRightOf = function(seq) {
		return this.oStart > seq.oEnd && this.nStart > seq.nEnd;
	};

	Sequence.prototype.isHeadLeftOf = function(seq) {
		return this.oStart < seq.oStart && this.nStart < seq.nStart;
	};

	Sequence.prototype.isTailRightOf = function(seq) {
		return this.oEnd > seq.oEnd && this.nEnd > seq.nEnd;
	};

	Sequence.prototype.isCompatibleTo = function(seq) {
		return this.isLeftOf(seq) || this.isRightOf(seq);
	};

	Sequence.prototype.grabItems = function(oArray, nArray) {
		switch(this.type) {
			case '-':
			case '=':
			this.items = oArray.slice(this.oStart, this.oNext);
			break;

			case '+':
			this.items = nArray.slice(this.nStart, this.nNext);
			break;
		}
	};

	Sequence.prototype.join = function() {
		return Array.prototype.join.apply(this.items, arguments);
	}

	Sequence.compareLength = function(s1, s2) {
		return s1.length - s2.length;
	}

	function cartesianProduct(key, oIndices, nIndices) {
		for(var i = oIndices.length; i--; ) {
			var oIndex = oIndices[i];

			for(var j = nIndices.length; j--; ) {
				var nIndex = nIndices[j];
				(this[oIndex] || (this[oIndex] = []))[nIndex] = 1;
			}
		}
	}

	function findCommonSequences(crossMatrix) {
		for(var i = crossMatrix.length; --i > 0; ) {
			var	row = crossMatrix[i],
				nextRow = crossMatrix[i - 1];

			if(!row || !nextRow) continue;

			for(var j = row.length; --j > 0 ; ) {
				if(row[j] > 0 && nextRow[j - 1] > 0) {
					nextRow[j - 1] += row[j];
					delete row[j];
				}
			}

			if(row.length === 0)
				delete crossMatrix[i];
		}

		var sequences = [];

		for(var i = crossMatrix.length; i--; ) {
			var row = crossMatrix[i];
			if(!row) continue;

			for(var j = row.length; j--; ) {
				if(row[j] > 0)
					sequences.push(new Sequence('=', i, j, row[j]));
			}
		}

		return sequences;
	}

	function compareSequencesByHead(s1, s2) {
		return s1.oStart - s2.oStart || s1.nStart - s2.nStart;
	}

	function filterCompatibleSequences(commonSequences) {
		commonSequences.sort(Sequence.compareLength);
		var sequences = [];

		outerLoop:
		for(var i = commonSequences.length; i--; ) {
			var current = commonSequences[i]

			innerLoop:
			for(var j = 0, len = sequences.length; j < len; ++j) {
				var dominant = sequences[j];

				if(current.isCompatibleTo(dominant))
					continue innerLoop;

				if(current.isHeadLeftOf(dominant)) {
					while(current.pop()) {
						if(current.isLeftOf(dominant))
							continue innerLoop;
					}
				}
				else if(current.isTailRightOf(dominant)) {
					while(current.shift()) {
						if(current.isRightOf(dominant))
							continue innerLoop;
					}
				}

				continue outerLoop;
			}

			sequences.push(current);
		}

		return sequences.sort(compareSequencesByHead);
	}

	function insertUnretainedSequences(retainedSequences, oLen, nLen) {
		var	oIndex = 0,
			nIndex = 0,
			sequences = [];

		for(var i = 0, len = retainedSequences.length; i < len; ++i) {
			var current = retainedSequences[i];

			if(oIndex < current.oStart) {
				sequences.push(new Sequence('-', oIndex, -1,
					current.oStart - oIndex));
			}

			if(nIndex < current.nStart) {
				sequences.push(new Sequence('+', -1, nIndex,
					current.nStart - nIndex))
			}

			sequences.push(current);
			oIndex = current.oNext;
			nIndex = current.nNext;
		}

		if(oIndex < oLen)
			sequences.push(new Sequence('-', oIndex, -1, oLen - oIndex));

		if(nIndex < nLen)
			sequences.push(new Sequence('+', -1, nIndex, nLen - nIndex));

		return sequences;
	}

	function grabItems(sequences, oArray, nArray) {
		for(var i = sequences.length; i--; )
			sequences[i].grabItems(oArray, nArray);

		return sequences;
	}

	function diff(oArray, nArray) {
		var	oTable = Map.reverseIndexTableFrom(oArray),
			nTable = Map.reverseIndexTableFrom(nArray),
			crossMatrix = Map.cross(oTable, nTable, cartesianProduct, []),
			sequences = findCommonSequences(crossMatrix);

		oTable = nTable = crossMatrix = null;

		sequences = filterCompatibleSequences(sequences);
		sequences = insertUnretainedSequences(sequences,
						oArray.length, nArray.length);
		sequences = grabItems(sequences, oArray, nArray);

		return sequences;
	}

	return diff;
})();