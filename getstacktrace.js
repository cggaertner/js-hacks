var getStackTrace = (function () {
	var	nlRegex = /\n/,
		stackRegex = /^((.+?)\(\))?@(.+?):(\d+)$/,
		msgRegex = /^\s*Line (\d+) of (inline#\d+ script in|linked script) (.+?)(: In function (.+?))?\s*$/,
		trimRegex = /^\s+|\s+$/g;

	function StackFrame(func, file, line, statement) {
		this.func = func;
		this.file = file;
		this.line = line;
		this.statement = statement;
	}

	StackFrame.prototype.toString = function() {
		return (this.func ? this.func + '() ' : '') +
			(this.file ? 'at '  + this.file : '') +
			(this.line ? ', line ' + this.line : '') +
			(this.statement ? ', statement ' + this.statement : '');
	}

	function parseStack(stack) {
		var trace = stack.split(nlRegex);
		trace.shift();

		for(var i = 0; i < trace.length; ++i) {
			var matches = trace[i].match(stackRegex);

			if(matches)
				trace[i] = new StackFrame(matches[2], matches[3], matches[4]);
			else trace.splice(i--, 1);
		}

		return trace;
	}

	function parseOperaMessage(msg) {
		var	trace = [],
			lines = msg.split(nlRegex);

		for(var i = 4; i < lines.length; i += 2) {
			var matches = lines[i].match(msgRegex);

			if(matches) {
				trace.push(new StackFrame(matches[5], matches[3], matches[1],
					lines[i + 1].replace(trimRegex, '')));
			}
		}

		return trace;
	}

	function createTrace(caller) {
		var trace = [];

		for(var current = caller; current; current = current.caller) {
			var src = current.toString();
			trace.push(new StackFrame(src.substring(src.indexOf('function') + 8,
				src.indexOf('(')).replace(trimRegex, '')));
		}

		trace.push(new StackFrame(undefined, location.href));
		return trace;
	}

	function toString() {
		return this.join('\n');
	}

	function getStackTrace() {
		try {
			(0)();
		}
		catch(e) {
			var trace = e.stack ? parseStack(e.stack) :
				window.opera && e.message ? parseOperaMessage(e.message) :
				createTrace(arguments.callee.caller);

			trace.toString = toString;
			return trace;
		}
	}

	return getStackTrace;
})();