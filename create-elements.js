if(typeof create !== 'function')
	throw new Error('`create-elements` requires `create`');

create.br = function() {
	return create('br');
};

create.div = function() {
	return create.appendix(create('div'), arguments)
};

create.fieldset = function(name) {
	var set = create('fieldset', null, create.legend(name));
	create.appendix(set, arguments, 1);
	return set;
};

create.h1 = function() {
	return create.appendix(create('h1'), arguments)
};

create.input = function(type, name, value) {
	var input = create('input', { 'type' : type });
	if(name) input.name = name;
	if(value) input.value = value;
	return input;
};

create.label = function(forId) {
	return create.appendix(create('label', { 'for' : forId }), arguments, 1);
};

create.legend = function(text) {
	return create('legend', null, create.from(text, false));
};

create.span = function() {
	return create.appendix(create('span'), arguments);
};

create.text = function(text) {
	return create.from(text, false);
};

create.fragment = function() {
	return create.appendix(document.createDocumentFragment(), arguments);
};