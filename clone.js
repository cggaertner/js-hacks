function clone(obj) {
	if(typeof obj !== 'undefined') {
		clone.prototype = Object(obj);
		return new clone;
	}
}