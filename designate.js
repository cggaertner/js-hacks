function designate(/* checks..., func */) {
	var	args = arguments,
		count = arguments.length - 1,
		func = arguments[count];

	return function() {
		for(var i = 0; i < count; ++i) {
			var check = args[i];
			if(typeof check === 'function' && !check.apply(arguments[i]))
				throw new Error('invalid arguments[' + i + ']');
		}

		return func.apply(this, arguments);
	};
}