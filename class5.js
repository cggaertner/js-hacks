// Copyright 2011 Christoph Gärtner
// Distributed under the Boost Software License, Version 1.0
// See <license.txt> or <http://www.boost.org/LICENSE_1_0.txt> for details

function Class() {}

Class.mixin = function() {
	for(var i = 0, len = arguments.length; i < len; ++i) {
		var members = arguments[i];
		for(var name in members) {
			if(members.hasOwnProperty(name)) {
				Object.defineProperty(this.prototype, name, {
					value : members[name],
					writable : true,
					enumerable : false,
					configurable : true
				});
			}
		}
	}

	return this;
};

Class.extend = function(members) {
	members = Object(members);

	if(!members.hasOwnProperty('constructor')) {
		members.constructor = function() {
			Base.apply(this, arguments);
		}
	}

	var	Base = this,
		Sub = members.constructor;

	Sub.extend = Class.extend;
	Sub.mixin = Class.mixin;

	Sub.prototype = Object.create(Base.prototype);
	Sub.mixin(members);

	return Sub;
};
