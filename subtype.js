if(!Function.prototype.subtype) Function.prototype.subtype = (function() {
	function Dummy() {}
	return function() {
		Dummy.prototype = this.prototype;
		return new Dummy;
	};
})();