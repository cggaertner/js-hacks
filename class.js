// Copyright 2011 Christoph Gärtner
// Distributed under the Boost Software License, Version 1.0
// See <license.txt> or <http://www.boost.org/LICENSE_1_0.txt> for details

function Class() {}
(function() {
	function Dummy() {}

	function mixin() {
		for(var i = 0, len = arguments.length; i < len; ++i) {
			var members = arguments[i];
			for(var name in members) {
				if(members.hasOwnProperty(name))
					this.prototype[name] = members[name];
			}

			// IE bugfix
			if(members.hasOwnProperty('constructor'))
				this.prototype.constructor = members.constructor;
		}

		return this;
	}

	function extend(members) {
		members = Object(members);

		if(!members.hasOwnProperty('constructor')) {
			members.constructor = function() {
				Base.apply(this, arguments);
			}
		}

		var	Base = this,
			Sub = members.constructor;

		Sub.extend = extend;
		Sub.mixin = mixin;

		Dummy.prototype = Base.prototype;
		Sub.prototype = new Dummy;
		Sub.mixin(members);

		return Sub;
	}

	Class.extend = extend;
	Class.mixin = mixin;
})();
