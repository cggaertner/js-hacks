(function() {

	if('a'.split(/(a)/)[1] === 'a')
		return;

	var nativeSplit = String.prototype.split;

	function classof(obj) {
		return /^\[object (.+)\]$/.exec(
			Object.prototype.toString.call(obj))[1];
	}

	function split(exp, limit) {
		if(typeof exp === 'string' || classof(exp) === 'String')
			return nativeSplit.apply(this, arguments);

		if(classof(exp) !== 'RegExp')
			throw new Error('illegal first argument to split()');

		if(limit === 0)
			return [];

		exp = new RegExp(exp.source, 'g' + (exp.ignoreCase ? 'i' : '') +
			(exp.multiline ? 'm' : ''));

		var	limit = limit || Infinity;
			count = 0,
			pos = 0,
			tokens = [];

		for(var match; count < limit && (match = exp.exec(this));
			pos = exp.lastIndex, ++count) {
			tokens.push(this.substring(pos, exp.lastIndex - match[0].length));

			for(var i = 1; i < match.length; ++i, ++count)
				tokens.push(match[i]);
		}

		if(count < limit && pos < this.length)
			tokens.push(this.substring(pos));

		return tokens;
	}

	String.prototype.split = split;

})();