var html = new (function() {
	var dummy = document.createElement('div');

	this.parse = function(code) {
		var frag = document.createDocumentFragment();
		/*@cc_on document.documentElement.appendChild(dummy); @*/
		dummy.innerHTML = code;
		/*@cc_on document.documentElement.removeChild(dummy); @*/

		while(dummy.childNodes.length)
			frag.appendChild(dummy.childNodes[0]);

		return frag;
	};

	this.from = function(node, esc) {
		dummy.appendChild(node.cloneNode(true));
		return esc ? dummy.innerHTML.replace(/&/g, '&amp;').
			replace(/</g, '&lt;').replace(/>/g, '&gt;') : dummy.innerHTML;
	};
});