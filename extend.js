var extend = (function() {
	function Dummy() {}

	return function(Func, init) {
		Dummy.prototype = Func.prototype;
		var proto = new Dummy;
		if(init) init.apply(proto);
		return proto;
	};
})();