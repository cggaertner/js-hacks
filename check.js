function check(field, checks) {
	var names = field.className.split(/\s+/);

	for(var i = 0; i < names.length; ++i) {
		var	name = names[i],
			exp = checks[name];

		if(Object.prototype.toString.call(exp) === '[object RegExp]' &&
			!exp.test(field.value)) return name;
	}

	return false;
}