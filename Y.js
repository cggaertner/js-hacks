function Y(f) {
	var g = f(function() { return g.apply(this, arguments); });
	return g;
}

function realY(f) {
	return (function(x) { return x(x); })(function(y) {
		return f(function() {
			return y(y).apply(null, arguments);
		});
	});
}

function pseudoY(f) {
	return function g() {
		return f.apply(g, arguments);
	};
}
