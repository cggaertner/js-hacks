function updateTable(oldBody, data) {
	var newBody = oldBody.cloneNode(false);

	if(data.length) {
		var emptyRow = document.createElement('tr');
		for(var i = data[0].length; i--; ) {
			emptyRow.appendChild(document.createElement('td')).
				appendChild(document.createTextNode(' '));
		}

		for(var i = 0, count = data.length; i < count; ++i) {
			var	dataRow = data[i],
				tableRow = newBody.appendChild(emptyRow.cloneNode(true));

			for(var j = dataRow.length; j--; )
				tableRow.childNodes[j].firstChild.nodeValue = dataRow[j];
		}
	}

	oldBody.parentNode.replaceChild(newBody, oldBody);
}