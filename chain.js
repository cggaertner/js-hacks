function chain(f1, f2) {
	return typeof f1 !== 'function' ? f2 : function() {
		var	r1 = f1.apply(this, arguments),
			r2 = f2.apply(this, arguments);
		return typeof r1 === 'undefined' ? r2 : (r1 && r2);
	};
}