if(	typeof window.XMLHttpRequest === 'undefined' &&
	typeof window.ActiveXObject === 'function') {
	window.XMLHttpRequest = function() {
		try { return new ActiveXObject('Msxml2.XMLHTTP.6.0'); } catch(e) {}
		try { return new ActiveXObject('Msxml2.XMLHTTP.3.0'); } catch(e) {}
		return new ActiveXObject('Microsoft.XMLHTTP');
	};
}