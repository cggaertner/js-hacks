function getHttpResource(url)  {
	var req = new XMLHttpRequest;
	if(!req) return null;

	try {
		req.open('GET', url, false);
		req.send(null);

		return req.responseText;
	}
	catch(e) {
		return null;
	}
}