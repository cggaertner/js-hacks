function toArray(obj) {
	if(obj.length !== obj.length >>> 0)
		throw new Error('illegal argument: invalid length property');

	var array = [];

	for(var i = obj.length; i--;) {
		if(i in obj)
			array[i] = obj[i];
	}

	return array;
}