function serialize(value, foreignProps, noErrors) {
	if(typeof quote !== 'function')
		throw new Error('`serialize` requires `quote`');

	var type = typeof value;

	if(type === 'undefined')
		return 'undefined';

	if((type === 'boolean' || value instanceof Boolean) ||
		(type === 'number' || value instanceof Number))
		return value.toString();

	if(type === 'string' || value instanceof String)
		return quote(value);

	if(type === 'object') {
		if(value === null)
			return 'null';

		if(value instanceof Array) {
			var entries = [];

			for(var i = 0; i < value.length; ++i)
				entries[i] = serialize(value[i], foreignProps, noErrors);

			return '[' + entries.join(',') + ']';
		}

		if(value instanceof Object) {
			var properties = [];

			for(var prop in value) {
				if(foreignProps || value.hasOwnProperty(prop)) {
					properties.push(quote(prop) + ':' +
						serialize(value[prop], foreignProps, noErrors));
				}
			}

			return '{' + properties.join(',') + '}';
		}

		if(noErrors)
			return 'undefined';

		throw new Error('can\'t serialize non-Object objects');
	}

	if(noErrors)
		return 'undefined';

	throw new Error('can\'t serialize values of type ' + type);
}