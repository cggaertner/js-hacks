function query(selector, parent) {
	if(selector.charAt(0) === '#')
		return document.getElementById(selector.substring(1));

	if(!parent)
		parent = document;

	if(selector.indexOf('.') < 0)
		return parent.getElementsByTagName(selector);

	var	tokens = selector.split('.'),
		tagName = tokens.unshift() || '*',
		elements = parent.getElementsByTagName(tagName),
		regs = [],
		matchingElements = [];

	for(var i = 0; i < tokens.length; ++i)
		regs.push(eval('/(^|\s)' + tokens[i] + '(\s|$)/'));

	loop:
	for(var i = 0; i < elements.length; ++i) {
		var element = elements[i];

		if(element.nodeType != 1)
			continue;

		for(var j = 0; j < regs.length; ++j) {
			if(!regs[j].test(element.className))
				continue loop;
		}

		matchingElements.push(element);
	}

	return matchingElements;
}