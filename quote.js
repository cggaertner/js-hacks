function quote(string, doubles, nonAscii) {
	var	len		= string.length,
		qchar	= doubles ? '"' : '\'',
		qString	= qchar;

	for(var current, i = 0; i < len; ++i) {
		current = string.charAt(i);

		if(' ' <= current && (nonAscii || current <= '~')) {
			if(current === '\\' || current === qchar)
				qString += '\\';

			qString += current;
		}
		else switch(current) {
			case '\b':
				qString += '\\b'
				break;

			case '\f':
				qString += '\\f'
				break;

			case '\n':
				qString += '\\n'
				break;

			case '\r':
				qString += '\\r'
				break;

			case '\t':
				qString += '\\t'
				break;

			case '\v':
				qString += '\\v'
				break;

			default:
				qString += '\\u';
				current = current.charCodeAt(0).toString(16);
				for(var j = 4; --j >= current.length; qString += '0');
				qString += current;
		}
	}

	qString += qchar;
	return qString;
}