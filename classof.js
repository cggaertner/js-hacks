// Copyright 2011 Christoph Gärtner
// Distributed under the Boost Software License, Version 1.0
// See <license.txt> or <http://www.boost.org/LICENSE_1_0.txt> for details

function classof(fn) {
	return new Class(fn);
}

function Class(fn) {
	this.fn = fn;
}

Class.prototype = new (function() {
	function Dummy() {}

	this.extend = function(baseFn) {
		Dummy.prototype = baseFn.prototype;
		this.fn.prototype = new Dummy;
		return this;
	};

	this.mixin = function(members) {
		for(var prop in members) {
			if(members.hasOwnProperty(prop))
				this.fn.prototype[prop] = members[prop];
		}
		return this;
	};
});
