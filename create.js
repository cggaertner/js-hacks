var create = (function() {

	var fallback = {
		'class' : 'className',
		'for' : 'htmlFor',
		'name' : 'name'
	};

	function create(name, attributes) {
		return set(document.createElement(name), attributes, arguments);
	}

	function set(element, attributes, args) {
		var fallback = create.fallback;

		for(var aName in attributes) {
			if(attributes.hasOwnProperty(aName)) {
				var value = attributes[aName];

				if(typeof value === 'string') {
					if(fallback.hasOwnProperty(aName))
						element[fallback[aName]] = value;
					else element.setAttribute(aName, value);
				}
				else if(value instanceof Object) {
					if(fallback.hasOwnProperty(aName))
						aName = fallback[aName];

					for(var prop in value) {
						if(value.hasOwnProperty(prop))
							element[aName][prop] = value[prop];
					}
				}
				else throw new Error('illegal attribute: ' + value);
			}
		}

		return append(element, args, 2);
	};

	function createFrom(html, attributes) {
		if(attributes === false)
			return document.createTextNode(html);

		var element = document.createElement('div');
		element.innerHTML = html;

		var	nodes = element.childNodes;
		for(var i = nodes.length; i--; )
			set(nodes[i], attributes, []);

		var fragment = document.createDocumentFragment();
		while(nodes.length)
			fragment.appendChild(element.removeChild(element.lastChild));

		return fragment;
	}

	function append(element, nodes, offset) {
		for(var i = offset || 0, len = nodes.length; i < len; ++i)
			element.appendChild(nodes[i]);

		return element;
	}

	create.fallback = fallback;
	create.from = createFrom;
	create.appendix = append;

	return create;
})();