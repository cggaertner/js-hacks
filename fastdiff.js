// still buggy - some input will loop forever!

var fastDiff = (function() {
	if(typeof Map !== 'function')
		throw new Error('`diff` requires `map`');

	function diff(oArray, nArray) {
		if(oArray.length === 0 && nArray.length === 0)
			return [];

		if(nArray.length === 0)
			return [sequence('-', oArray, 0, oArray.length)];

		if(oArray.length === 0)
			return [sequence('+', nArray, 0, nArray.length)];

		var	nTable = Map.reverseIndexTableFrom(nArray),
			oTable = Map.reverseIndexTableFrom(oArray),
			nCrossList = [],
			oCrossList = [];

		Map.cross(nTable, oTable, function(entry, nIndices, oIndices) {
			if(nIndices.length === 1 && oIndices.length === 1) {
				nCrossList[nIndices[0]] = oIndices[0];
				oCrossList[oIndices[0]] = nIndices[0];
			}
		});

		for(var i = 0; i < nArray.length - 1; ++i) {
			var	nIdx = i + 1,
				oIdx = nCrossList[i] + 1;

			if(nCrossList[i] !== undefined && nCrossList[nIdx] === undefined &&
				oIdx < oArray.length && oCrossList[oIdx] === undefined &&
				nArray[nIdx] === oArray[oIdx]) {
				nCrossList[nIdx] = oIdx;
				oCrossList[oIdx] = nIdx;
			}
		}

		for(var i = nArray.length; --i > 0;) {
			var	nIdx = i - 1,
				oIdx = nCrossList[i] - 1;

			if(nCrossList[i] !== undefined && nCrossList[nIdx] === undefined &&
				oIdx >= 0 && oCrossList[oIdx] === undefined &&
				nArray[nIdx] === oArray[oIdx]) {
				nCrossList[nIdx] = oIdx;
				oCrossList[oIdx] = nIdx;
			}
		}

		var	list = [],
			nIdx = 0,
			oIdx = 0;

		var nStart, oStart;

// TODO: fix the loop!

		while(nIdx < nArray.length && oIdx < oArray.length) {
			for(oStart = oIdx; oIdx < oArray.length &&
				oCrossList[oIdx] === undefined; ++oIdx);

			if(oStart < oIdx)
				list.push(sequence('-', oArray, oStart, oIdx));

			for(nStart = nIdx; nIdx < nArray.length &&
				nCrossList[nIdx] === undefined; ++nIdx);

			if(nStart < nIdx)
				list.push(sequence('+', nArray, nStart, nIdx));

			for(oStart = oIdx, nStart = nIdx;
				nIdx < nArray.length && oIdx < oArray.length &&
				(oIdx === nCrossList[nIdx] && nIdx === oCrossList[oIdx]);
				++oIdx, ++nIdx);

			if(oStart < oIdx)
				list.push(sequence('=', oArray, oStart, oIdx));
		}

		if(oIdx < oArray.length)
			list.push(sequence('-', oArray, oIdx, oArray.length));

		if(nIdx < nArray.length)
			list.push(sequence('+', nArray, nIdx, nArray.length));

		return list;
	}

	function sequence(type, array, start, end) {
		var seq = array.slice(start, end);
		seq.type = type;
		return seq;
	}

	return diff;
})();