function delay(func, timeout) {
	return function() {
		var self = this, args = arguments;
		setTimeout(function() { func.apply(self, args); }, timeout);
	};
}
