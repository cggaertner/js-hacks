function preloadImages(images, listener) {
	var remaining, i;

	function loaded() {
		listener(--remaining);
	}

	remaining = i = images.length;

	while(i--) {
		var img = new Image;
		if(listener)
			img.onload = loaded;
		img.src = images[i];
	}
}