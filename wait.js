function wait(seconds) {
	if(this instanceof wait)
		this.delay = seconds;
	else return new wait(seconds);
}

wait.prototype.then = function(callback) {
	setTimeout(callback, this.delay * 1000);
	return this;
};

wait.prototype.wait = function(seconds) {
	this.delay += seconds;
	return this;
};