var fibonacci = (function() {

	var loop = (function() {
		function fib(n) {
			var	i = 0, j = 1;

			while(n--) {
				var tmp = i;
				i = j;
				j += tmp;
			}

			return i;
		}

		fib.name = 'loop';
		return fib;
	})();

	var round = (function() {
		var	sqrt_5 = Math.sqrt(5),
			phi = (1 + sqrt_5) / 2;

		function fib(n) {
			return Math.floor(Math.pow(phi, n) / sqrt_5 + 0.5);
		}

		fib.name = 'round';
		return fib;
	})();

	var matrix = (function() {
		function mult(a, b) {
			return [
				[a[0][0] * b[0][0] + a[0][1] * b[1][0],
					a[0][0] * b[0][1] + a[0][1] * b[1][1]],
				[a[1][0] * b[0][0] + a[1][1] * b[1][0],
					a[1][0] * b[0][1] + a[1][1] * b[1][1]]
			];
		}

		function pow(a, n) {
			if(n < 2)
				return n === 1 ? a : [[1, 0], [0, 1]];

			var	a_pow_n_2 = pow(a, n >> 1),
				a_pow_n_even = mult(a_pow_n_2, a_pow_n_2);

			return n & 1 ? mult(a, a_pow_n_even) : a_pow_n_even;
		}

		function fib(n) {
			return n >= 1 ? pow([[1, 1], [1, 0]], n - 1)[0][0] : 0;
		}

		fib.name = 'matrix';
		return fib;
	})();

	var matrix_destructive = (function() {
		var base = [[1, 1], [1, 0]];

		function mult(a, b) {
			var	a00 = a[0][0],
				a01 = a[0][1],
				a10 = a[1][0],
				a11 = a[1][1],
				b00 = b[0][0],
				b01 = b[0][1],
				b10 = b[1][0],
				b11 = b[1][1];

			a[0][0] = a00 * b00 + a01 * b10;
			a[0][1] = a00 * b01 + a01 * b11;
			a[1][0] = a10 * b00 + a11 * b10;
			a[1][1] = a10 * b01 + a11 * b11;
		}

		function pow(a, n) {
			if(n < 2)
				return n === 1 ? a : [[1, 0], [0, 1]];

			pow(a, n >> 1);
			mult(a, a);

			if(n & 1)
				mult(a, base);

			return a;
		}

		function fib(n) {
			return n >= 1 ? pow([[1, 1], [1, 0]], n - 1)[0][0] : 0;
		}

		fib.name = 'matrix_destructive';
		return fib;
	})();

	var matrix_optimized = (function() {
		function square(a) {
			var	a00 = a[0][0],
				a01 = a[0][1],
				a10 = a[1][0],
				a11 = a[1][1];

			var	a10_x_a01 = a10 * a01,
				a00_p_a11 = a00 + a11;

			a[0][0] = a10_x_a01 + a00 * a00;
			a[0][1] = a00_p_a11 * a01;
			a[1][0] = a00_p_a11 * a10;
			a[1][1] = a10_x_a01 + a11 * a11;
		}

		function powPlusPlus(a) {
			var	a01 = a[0][1],
				a11 = a[1][1];

			a[0][1] = a[0][0];
			a[1][1] = a[1][0];
			a[0][0] += a01;
			a[1][0] += a11;
		}

		function pow(a, n) {
			if(n < 2)
				return n === 1 ? a : [[1, 0], [0, 1]];

			pow(a, n >> 1);
			square(a);

			if(n & 1)
				powPlusPlus(a);

			return a;
		}

		function fib(n) {
			return n >= 1 ? pow([[1, 1], [1, 0]], n - 1)[0][0] : 0;
		}

		fib.name = 'matrix_optimized';
		return fib;
	})();

	var loop_cached = (function () {
		var cache = [0, 1];

		function fib(n) {
			if(n >= cache.length) {
				for(var i = cache.length; i <= n; ++i)
					cache[i] = cache[i - 1] + cache[i - 2];
			}

			return cache[n];
		}

		fib.name = 'loop_cached';
		return fib;
	})();

	var matrix_cached = (function() {
		var cache = {};
		cache[0] = [[1, 0], [0, 1]];
		cache[1] = [[1, 1], [1, 0]];

		function mult(a, b) {
			return [
				[a[0][0] * b[0][0] + a[0][1] * b[1][0],
					a[0][0] * b[0][1] + a[0][1] * b[1][1]],
				[a[1][0] * b[0][0] + a[1][1] * b[1][0],
					a[1][0] * b[0][1] + a[1][1] * b[1][1]]
			];
		}

		function compute(n) {
			if(!cache[n]) {
				var n_2 = n >> 1;
				compute(n_2);
				cache[n] = mult(cache[n_2], cache[n_2]);
				if(n & 1)
					cache[n] = mult(cache[1], cache[n]);
			}
		}

		function fib(n) {
			if(n == 0)
				return 0;

			compute(--n);

			return cache[n][0][0];
		}

		fib.name = 'matrix_cached';
		return fib;
	})();

	var precomputed = (function() {
		var fibs = [
			0,
			1,
			1,
			2,
			3,
			5,
			8,
			13,
			21,
			34,
			55,
			89,
			144,
			233,
			377,
			610,
			987,
			1597,
			2584,
			4181,
			6765,
			10946,
			17711,
			28657,
			46368,
			75025,
			121393,
			196418,
			317811,
			514229,
			832040,
			1346269,
			2178309,
			3524578,
			5702887,
			9227465,
			14930352,
			24157817,
			39088169,
			63245986,
			102334155,
			165580141,
			267914296,
			433494437,
			701408733,
			1134903170,
			1836311903,
			2971215073,
			4807526976,
			7778742049,
			12586269025,
			20365011074,
			32951280099,
			53316291173,
			86267571272,
			139583862445,
			225851433717,
			365435296162,
			591286729879,
			956722026041,
			1548008755920,
			2504730781961,
			4052739537881,
			6557470319842,
			10610209857723,
			17167680177565,
			27777890035288,
			44945570212853,
			72723460248141,
			117669030460994,
			190392490709135,
			308061521170129,
			498454011879264,
			806515533049393,
			1304969544928657,
			2111485077978050,
			3416454622906707,
			5527939700884757,
			8944394323791464
		];

		function fib(n) {
			return fibs[n];
		}

		fib.name = 'precomputed';
		return fib;
	})();

	function resultToString() {
		return this.name + '(' + (this.range ? '0..' : '') + this.num +
			') = ' + this.value + ' [' + this.count + 'x in ' +
			this.time + 'ms]';
	}

	function resultsToString() {
		return this.join('\n');
	}

	function benchmark(num, count, range) {
		var funcs = [];

		for(var i = 3; i < arguments.length; ++i)
			funcs.push(arguments[i]);

		if(funcs.length === 0)
			funcs = algorithms;

		var results, fib, value, start, end;
		results = [];

		for(var i = 0, len = funcs.length; i < len; ++i) {
			fib = funcs[i];
			start = new Date;
			for(var j = count; j--; ) {
				if(range) {
					for(var k = 0; k <= num; ++k)
						value = fib(k);
				}
				else value = fib(num);
			}
			end = new Date;
			results[i] = {
				name : fib.name,
				time : Number(end) - Number(start),
				num : num,
				count : count,
				value : value,
				range : range
			};
			results[i].toString = resultToString;
		}

		results.toString = resultsToString;
		return results;
	}


	var algorithms = [
		loop,
		round,
		matrix,
		matrix_destructive,
		matrix_optimized,
		loop_cached,
		matrix_cached,
		precomputed
	];

	var fibonacci = {
		benchmark : benchmark,
		algorithms : algorithms
	};

	for(var i = 0; i < algorithms.length; ++i)
		fibonacci[algorithms[i].name] = algorithms[i];

	return fibonacci;
})();