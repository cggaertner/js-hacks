function Tuple(a, b, __hash) {
	this.a = a;
	this.b = b;
	if(__hash)
		this.__hash =__hash;
	else this.modified();
}

Tuple.from = function(a, b) {
	return new Tuple(Number(a), Number(b));
};

Tuple.prototype.modified = function() {
	this.__hash = 'tuple (' + this.a + ':' + this.b + ')';
};

Tuple.prototype.set = function(a, b) {
	this.a = a;
	this.b = b;
	this.modified();
	return this;
};

Tuple.prototype.move = function(dA, dB) {
	this.a += dA;
	this.b += dB;
	this.modified();
	return this;
};

Tuple.prototype.toString = function() {
	return '(' + this.a + ':' + this.b + ')'
};

Tuple.prototype.copy = function() {
	return new Tuple(this.a, this.b, this.__hash);
}

Tuple.compareLeft = function(t1, t2) {
	return t1.a - t2.a || t1.b - t2.b;
};

Tuple.compareRight = function(t1, t2) {
	return t1.b - t2.b || t1.a - t2.a;
};

Tuple.sign = function(x) {
	return (x > 0) - (x < 0);
};

Tuple.compare = function(t1, t2) {
	var	sA = Tuple.sign(t1.a - t2.a),
		sB = Tuple.sign(t1.b - t2.b);

	if((sA < 0 && sB > 0) || (sA > 0 && sB < 0))
		return NaN;

	return sA || sB;
};