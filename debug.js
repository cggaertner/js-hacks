var debug = (function() {

	function skip(tokens, idx, limiter, escapes) {
		while(++idx < tokens.length && tokens[idx] !== limiter)
			if(escapes && tokens[idx] === '\\') ++idx;

		return idx;
	}

	function parse(func, callback) {
		var	regex = /\bassert:|\/\*|\*\/|\/\/|\\|"|'|\n|\s+/mg,
			tokens = [],
			pos = 0,
			code = func.toString();

		for(var matches; matches = regex.exec(code); pos = regex.lastIndex) {
			var	match = matches[0],
				matchStart = regex.lastIndex - match.length;

			if(pos < matchStart)
				tokens.push(code.substring(pos, matchStart));

			tokens.push(match);
		}

		if(pos < code.length)
			tokens.push(code.substring(pos));

		for(var i = 0; i < tokens.length; ++i) {
			switch(tokens[i]) {
				case '/*':
				i = skip(tokens, i, '*/');
				break;

				case '//':
				i = skip(tokens, i, '\n');
				break;

				case '"':
				i = skip(tokens, i, '"', true);
				break;

				case '\'':
				i = skip(tokens, i, '\'', true);
				break;

				case 'assert:':
				var j = callback(tokens, i);
				if(typeof j === 'number') i = j;
				break;
			}
		}

		return tokens;
	}


	function insertHook(tokens, idx) {
		var current = idx;
		while(/^\s+$/.test(tokens[++current]));

		if(tokens[current] !== '"' && tokens[current] !== '\'')
			throw new SyntaxError('illegal assert statement');

		var start = current;
		current = skip(tokens, current, tokens[current], true);
		var condition = tokens.slice(start, ++current).join('');
		tokens.splice(idx, current - idx,
			'(debug.assert(' + condition + ', eval(' + condition +')))');
	}

	function countAssertions(func) {
		var count = 0;
		parse(func, function() { ++count; });
		return count;
	}

	function throwError(condition) {
		throw new Error('assertion failed: ' + condition);
	}

	function noop() {}

	function debug(func, pass, fail) {
		if(typeof func !== 'function')
			throw new Error('illegal argument');

		debug.assert = function assert(condition, value) {
			if(value) (pass || noop)(condition);
			else (fail || throwError)(condition);
			return condition;
		};

		eval('func = (' + parse(func, insertHook).join('') + ')');
		return func;
	}

	function run(TestSuite) {
		var	suite = new (debug(TestSuite, function() { ++passed; })),
			unhookedSuite = new TestSuite,
			testsPassed = 0,
			testsCount = 0;

		for(var name in suite) {
			var func = suite[name];

			if(suite.hasOwnProperty(name) && typeof func === 'function') {
				var	passed = 0,
					count = countAssertions(unhookedSuite[name]);

				this.log('--- ' + name + ' ---');

				try {
					func.call(suite);
					this.log('PASSED ' + passed + '/' + count +
						' assertions\n');
					++testsPassed;
				}
				catch(e) {
					this.log(e.message);
					this.log('FAILED after assertion ' + passed + '/' +
						count + '\n');
				}

				++testsCount;
			}
		}

		this.log('--- RESULT: ' +
			(testsPassed === testsCount ? 'SUCCESS' : 'FAILURE') + ' ---');
		this.log('passed ' + testsPassed + '/' + testsCount + ' unit tests');
	}

	debug.log = noop;
	debug.run = run;
	return debug;

})();